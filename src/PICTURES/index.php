<?php

if(isset($_GET["picture_id"])){
    include '../php/header.php';
    include '../php/connection.php';

    ?>

<div id="fh5co-main">
    <div class="fh5co-gallery">
        <?php
    $sql = "SELECT * FROM picture WHERE id = " . $_GET["picture_id"] . ";";

    $result = $conn->query($sql);
    if($result->num_rows > 0) {
        // output data of each row
        while ($row = $result->fetch_assoc()) {
            echo '<img class="fullImage" src="../PICTURES/' . $row["name"] . '">';
        }
    }

        ?>
    </div>
</div>

<?php
include '../php/footer.php';

} else {
    header("Location: ../HOME/");
}

?>