<?php

include '../php/header.php';
include '../php/connection.php';
?>

<div id="fh5co-main">
    <div class="fh5co-gallery">
        <i class="fas fa-eye"></i>
        <?php

        $sql = "SELECT * FROM gallery WHERE private = 0 ORDER BY Views DESC";

        $result = $conn->query($sql);
        if($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $id = $row['id'];
                $name = $row['name'];
                $fk_image = $row['fk_image'];
                $fk_user = $row['fk_user'];
                $timestamp = $row['timestamp'];
                $enabled = $row['enabled'];
                $private = $row['private'];
                $Views = $row['Views'];
                $image;
                $imagecount;
                $sql2 = "SELECT * FROM `picture` WHERE id = ". $fk_image .";";
                $result2 = $conn->query($sql2);
                if ($result2->num_rows > 0) {
                    while($row2 = $result2->fetch_assoc()) {
                        $image = $row2['name'];
                    }
                }

                $sql3 = "SELECT * FROM `gallery_picture` WHERE fk_gallery = " . $id . ";";
                $result3 = $conn->query($sql3);
                $imagecount = $result3->num_rows;

                echo '<a class="gallery-item" href="../GALLERY/index.php?galleryid=' . $id .'">';
                echo '<img src="../PICTURES/' . $image . '">';
                echo '<span class="overlay">';
                echo '<h2>' . $name . '</h2>';
                echo '<span>' . $imagecount . ' PHOTOS</span>';
                echo '<span><i class="icon-eye"></i> ' . $Views . '</span>';
                echo '</span>';
                echo '</a>';
            }
        } else {
            echo "no Gallaries";
        }

        ?>
    </div>
</div>

<?php
include '../php/footer.php';
?>
