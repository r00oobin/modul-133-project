<?php
include '../php/header.php';

?>
    <div id="fh5co-main">
        <div class="fh5co-narrow-content animate-box" data-animate-effect="fadeInLeft">
		<?= (isset($error)) ? $error : '' ; ?>
            <form action="validate.php" method="post">
                <div class="row register">
                    <h1>Register</h1>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group">
                                <input name="username" value="<?= (isset($_POST['username'])) ? $_POST['username'] : ''; ?>" type="text" class="form-control" placeholder="Username" required>
                            </div>
                            <div class="form-group">
                                <input name="email" value="<?= (isset($_POST['email'])) ? $_POST['email'] : ''; ?>" type="e-mail" class="form-control" placeholder="E-Mail" required>
                            </div>
                            <div class="form-group">
                                <input name="password" value="<?= (isset($_POST['password'])) ? $_POST['password'] : ''; ?>" type="password" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="form-group">
                                <input name="confirmPwd" value="<?= (isset($_POST['confirmPwd'])) ? $_POST['confirmPwd'] : ''; ?>" type="password" class="form-control" placeholder="Confirm password" required>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary btn-md" value="Log in">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php
include '../php/footer.php';
?>