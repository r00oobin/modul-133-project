<?php

include '../php/connection.php';

$flag = true;

if(isset($_POST['username'])){
    $username = $_POST['username'];

    $sql = "SELECT username FROM user WHERE username='$username'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $error = "<h4 style='text-align: center; color: red; margin-top: '>This username already exists. Please choose another one.</h4>";
		include 'index.php';
		
        $flag = false;
    }
} else if($flag){
    $username = "";
    $flag = false;
}
    if(isset($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
        $email = $_POST['email'];

        $sql = "SELECT email FROM user WHERE email='$email'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            $error = "<h4 style='text-align: center; color: red;'>This email already exists.</h4>";
			include 'index.php';
			
            $flag = false;
        }
    }else if($flag){
		$email = "";
		$error = "<h4 style='text-align: center; color: red;'>Please enter a valid email adress.</h4>";
		include 'index.php';
		
		$flag = false;
    }

if(isset($_POST['password']) && isset($_POST['confirmPwd'])){
    if($_POST['password'] == $_POST['confirmPwd']){
        $password = $_POST['password'];
        $confirmPwd = $_POST['confirmPwd'];
    }else if($flag){
        $error = "<h4 style='text-align: center; color: red;'>Please enter two equal passwords.</h4>";
		include 'index.php';
        $password = $_POST['password'];
        $confirmPwd = $_POST['confirmPwd'];
		
		
        $flag = false;
    }

    if(strlen($_POST['password']) >= 6 && preg_match('~[0-9]~', $_POST['password'])){
        $password = $_POST['password'];
        $confirmPwd = $_POST['confirmPwd'];
    } else if($flag){
        $error = "<h4 style='text-align: center; color: red;'>Your password must have at least 6 characters and a number.</h4>";
		include 'index.php';	
		
        $flag = false;
    }

} else {
    $password = "";
    $confirmPwd = "";
	include 'index.php';
	
    $flag = false;
}

if($flag){
    $sql = "INSERT INTO user (username, email, password, enabled)
			VALUES ('$username', '$email', '$password', false)";

    if ($conn->query($sql) === TRUE) {
        //TODO : header("Location: emailBestätigung.php");
        echo "<h4 style='text-align: center; color: green;'>User created successfully!</h4>";
    } else {
        $error = "<h4 style='text-align: center; color: green;'>Couldn't create user, try again.</h4>";
    }

    $conn->close();
}
?>