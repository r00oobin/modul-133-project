<?php
session_start();
include "../php/connection.php";

$uploadError = false;
$nameAlreadyExits = false;
$galleryid = null;
$name = $_POST["galleryName"];
$userid = $_SESSION["userid"];

$target_dir = "../PICTURES/";

$sql = "SELECT id, name FROM gallery WHERE name= '$name';";
$result = $conn->query($sql);
if ($result->num_rows != 0) {
    $errorMessage = "<h4 style='text-align: center; color: red;'>This gallery exists already. Please choose another name.</h4>";
    header("Location: ./newGallery.php?errorMessage=" . $errorMessage);
    $nameAlreadyExits = true;
    $uploadError = true;
}

if(isset($_FILES["pictures"]["name"])) {
        $pathinfo = pathinfo($_FILES["pictures"]["name"]);

    if(!(strtolower($pathinfo["extension"]) == "png" || strtolower($pathinfo["extension"]) == "jpg" || strtolower($pathinfo["extension"]) == "jpeg" || strtolower($pathinfo["extension"]) == "gif")){
            $extension = $pathinfo["extension"];
            $errorMessage = "<h4 style='text-align: center; color: red;'>Please choose a .png, .jpg or a .jpeg-File. Other files are not allowed.$extension</h4>";
            header("Location: ./newGallery.php?errorMessage=" . $errorMessage);
            $uploadError = true;
        } else if ($_FILES["pictures"]["size"] > 10485760) {
            $errorMessage = "<h4 style='text-align: center; color: red;'>One of your pictures is too big. Max. file-size is 10MB.</h4>";
            header("Location: ./newGallery.php?errorMessage=" . $errorMessage);
            $uploadError = true;
        }

        if(isset($_FILES["pictures"]["name"]) && !$nameAlreadyExits && !$uploadError) {
            $original_filename = $_FILES["pictures"]["name"];
            $date = new DateTime();
            $timestamp = $date->getTimestamp() . rand(10000, 99999);
            $target = "../PICTURES/" . $timestamp . "." . $pathinfo["extension"];
            $tmp  = $_FILES['pictures']['tmp_name'];
            echo "<pre>";
            print_r($_FILES["pictures"]);
            echo "</pre>";
            if(move_uploaded_file($tmp, $target)){
                $sql = "INSERT INTO picture (name, fk_user) VALUES ('" . $timestamp . "." . $pathinfo["extension"] . "', $userid);";
                $pictureid;
                if ($conn->query($sql) === FALSE) {
                    $errorMessage = "<h4 style='text-align: center; color: red;'>There was an error uploading your file. Please try again.</h4>";
                    header("Location: ./newGallery.php?errorMessage=" . $errorMessage);
                    $uploadError = true;
                }

                if(!$uploadError && !$nameAlreadyExits){

                    $sql = "SELECT id FROM picture WHERE name= '" . $timestamp . "." . $pathinfo["extension"] . "';";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $pictureid = $row["id"];
                        }
                    } else {
                        $errorMessage = "<h4 style='text-align: center; color: red;'>There was an error uploading your pictures. Please try again.</h4>";
                        header("Location: ./newGallery.php?errorMessage=" . $errorMessage);
                        $uploadError = true;
                    }
                    $sql = "INSERT INTO gallery (name, fk_user, fk_image) VALUES ('$name', $userid, $pictureid)";
                    if ($conn->query($sql)) {
                        $errorMessage = "<h4 style='text-align: center; color: green;'>Gallery created successfully!</h4>";
                        header("Location: ./newGallery.php?errorMessage=" . $errorMessage);
                    } else {
                        $errorMessage = "<h4 style='text-align: center; color: red;'>There was an error creating your gallery. Please try again.</h4>";
                        header("Location: ./newGallery.php?errorMessage=" . $errorMessage);
                        $uploadError = true;
                    }
                } else {
                    $errorMessage = "<h4 style='text-align: center; color: red;'>There was an error creating your gallery. Please try again.</h4>";
                    header("Location: ./newGallery.php?errorMessage=" . $errorMessage);
                    $uploadError = true;
                }
            } else {
                $errorMessage = "<h4 style='text-align: center; color: red;'>There was an error uploading your pictures. Please try again.</h4>";
                header("Location: ./newGallery.php?errorMessage=" . $errorMessage);
                $uploadError = true;
            }
        }
        $conn->close();
}
?>