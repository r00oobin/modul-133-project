
<?php

include '../php/header.php';
include '../php/connection.php';

if(!isset($_GET['galleryid'])){
    header("Location: ../HOME/");
}
$galleryid = $_GET['galleryid'];

$name;
$fk_image;
$fk_user;
$timestamp;
$enabled;
$private;
$Views;

?>

<div id="fh5co-main">
    <div class="fh5co-gallery">

        <?php

        $sql = "SELECT * FROM gallery WHERE id = '$galleryid';";

        $result = $conn->query($sql);
        if($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $id = $row['id'];
                $name = $row['name'];
                $fk_image = $row['fk_image'];
                $fk_user = $row['fk_user'];
                $timestamp = $row['timestamp'];
                $enabled = $row['enabled'];
                $private = $row['private'];
                $Views = $row['Views'];
            }
        }  else {
            header("Location: ../HOME/");
        }

        $Views++;
        $sql = "UPDATE gallery SET Views = $Views WHERE id = $id";
        if ($conn->query($sql)) {}
        $sql = "SELECT * FROM gallery_picture WHERE fk_gallery = " . $galleryid . ";";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $sql2 = "SELECT * FROM picture WHERE id = " . $row['fk_picture'] . ";";
                //echo $sql2;
                $result2 = $conn->query($sql2);
                if ($result2->num_rows > 0) {
                    while($row2 = $result2->fetch_assoc()) {
                        echo '<a class="gallery-item" href="../PICTURES/index.php?picture_id=' . $row2["id"] .'">';
                        echo '<img src="../PICTURES/' . $row2["name"] . '">';
                        echo '<span class="overlay">';
                        //$fileExtPos = $row2["name"].LastIndexOf(".");
                        //if ($fileExtPos >= 0 )
                         //   echo '<h2>' . $row2["name"].Substring(0, $fileExtPos) . '</h2>';
                        $new_datetime = DateTime::createFromFormat ( "Y-m-d H:i:s", $row2["timestamp"] );
                        echo '<span>' . $new_datetime->format('l jS F Y') . '</span>';
                        echo '</span>';
                        echo '</a>';
                    }
                }
            }
        } else {
            echo "<span>no Pictures</span>";
        }


        if(isset($_SESSION["LogIn"])){
            if($fk_user == $_SESSION["userid"]){
                ?>
                    <form id="addFilesForm" action="./addPicture.php" method="post" enctype="multipart/form-data">
                        <input type="file" accept="image/*" name="pictures[]" class="addIcon" multiple id="inputFiles" style="position: absolute; visibility: hidden;">
                        <label class="addIcon" for="inputFiles">+</label>
                        <input name="id" value="<?php echo $galleryid; ?>" style="position: absolute; visibility: hidden;">
                    </form>
                <?php
            }
        }
        ?>


    </div>
</div>

<script>
    var inputFiles = document.getElementById("inputFiles");

    inputFiles.addEventListener("change", function () {
        document.getElementById("addFilesForm").submit();
    })
</script>

<?php
include '../php/footer.php';
?>