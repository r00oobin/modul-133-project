<?php
include '../php/header.php';
?>
<div id="fh5co-main">
	<div class="fh5co-narrow-content animate-box" data-animate-effect="fadeInLeft">
        <?php
        if(isset($_GET["errorMessage"])){
            echo $_GET["errorMessage"];
        }
        ?>
		<form action="createGallery.php" method="post" enctype="multipart/form-data">
			<div class="row register">
				<h1>New gallery</h1>
				<div class="col-md-12">
					<div class="row">
						<div class="form-group">
							<input type="text" name="galleryName" class="form-control" placeholder="Galleryname" required>
						</div>
                        <div class="form-group">
                            <input type="file" accept="image/*" id="pictures" name="pictures" class="form-control" placeholder="Titlepicture" required>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary btn-md" value="Create gallery">
                        </div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<?php
include '../php/footer.php';
?>